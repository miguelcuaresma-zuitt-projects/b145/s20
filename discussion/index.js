// What are objects?
//is a collection of related data and/or functionality

//analogy 
// CSS - everything is a box
//JS - MOST things are objects

// example: cellphone is an object in the real world.
	//  it has its own properties (color, weight, unit)
	// it has its own functions (open, close, send, messages and etc.)

//how to create/instantiate an object in js
	// 1. Object literal
	// let objectName = {}

	// 2. Object constructor / object initializer
	// let objectName = Object()

	// let cellphone = {
	// 	color: "White",
	// 	weight: "115 grams",
	// 	// an object can also contain functions
	// 	alarm: function() {
	// 		console.log("Alarm is buzzing")
	// 	},
	// 	ring: function() {
	// 		console.log("Cellphone is ringing")
	// 	}
	// }

let cellphone = Object({
	color: "White",
	weight: "115 grams",
	// an object can also contain functions
	alarm: function() {
	console.log("Alarm is buzzing")
	},
	ring: function() {
	console.log("Cellphone is ringing")
	}
})
	console.log(cellphone)

// how to create an object as a blueprint
	// reusable function that will create several objects that have the same data structures.

	// this approact is very useful in creating multiplel instances/duplicates/copies of the same object.

/*
	syntax: function blueprintName(arg) {
	this.arg = argVal
} 
*/

function Laptop(name, manufactureDate, color) {
	this.pangalan = name;
	this.createdOn = manufactureDate;
	this.kulay = color;
}

//this - keyword allows us to assign a new object properties by associating the received values

// new - used to create an instance of a new object
let item1 = new Laptop("Lenovo", "2008", "black")
let item2 = new Laptop("Acer", "2019", "black")
console.log(item2)

function PokemonAnatomy(name, type, level, isShiny) {
	this.name = name
	this.type = type;
	this.level = level;
	this.isShiny = isShiny
	this.pokeHealth = 80 * level
	this.attack = function() {
		console.log(this.name + " use Tackle")
	}
}

let pikachu = new PokemonAnatomy("Pickachu", "Electric", 3, true)
let ratata = new PokemonAnatomy("Ratata", "Ground", 2, true)
let onyx = new PokemonAnatomy("Onyx", "Rock", 8, true)
let meowth = new PokemonAnatomy("Meowth", "Normal", 5, true)
let snorlax = new PokemonAnatomy("Snorlax", "Normal", 9, false) 

console.log(pikachu)